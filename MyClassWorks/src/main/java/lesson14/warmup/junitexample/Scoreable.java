package lesson14.warmup.junitexample;

public interface Scoreable {
    int getScore();
}
