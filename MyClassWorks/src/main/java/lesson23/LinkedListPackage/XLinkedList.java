package lesson23.LinkedListPackage;

import java.util.StringJoiner;

public class XLinkedList {

  static class Node {
    final int value;
    Node next;

    Node(int value) {
      this.value = value;
    }

    Node(int value, Node next) {
      this.value = value;
      this.next = next;
    }

  }

  Node head = null;

  // it modifies the structure and returns new head
  void prepend(int element) {
    Node node = new Node(element);
    node.next = head;
    head = node;
  }

  void append(int element) {
    Node node = new Node(element);
    if (head == null) {
      head = node;
    } else {
      Node curr = head;
      while (curr.next != null) {
        curr = curr.next;
      }
      curr.next = node;
    }
  }

  int size() {
    throw new IllegalArgumentException("size:hasn't implemented yet");
  }

  boolean contains(int element){
    throw new IllegalArgumentException("contains:hasn't implemented yet");
  }

  Node reverse() {
    throw new IllegalArgumentException("reverse:hasn't implemented yet");
  }
  String represent(){
    StringJoiner sj = new StringJoiner(",", "(", ")");
    Node curr = head;
    while (curr != null) {
      sj.add(String.valueOf(curr.value));
      curr = curr.next;
    }
    return sj.toString();
  }

  private void attach_next(Node curr, StringJoiner sj){
      if(curr!=null){
        sj.add(String.valueOf(curr.value));
        attach_next(curr.next,sj);
      }
  }

  ////////////////////////////////////////////////////////////////////////The task
  String represent2(){
    StringJoiner sj = new StringJoiner(",", "(", ")");
    Node curr = head;
    attach_next(head,sj);
    return sj.toString();
  }
}
